
package snake5000

import primesnakesolver.PrimeSnakeSolver

import scala.collection.immutable.HashSet

object SnakeAnalyzer extends App {

  val allMoves = args(0)
  val snakeLength = args(1).toInt

  val primes = Primes.list(snakeLength)
  val diffs = (List(0) ++ primes).zip(primes).map { pair => pair._2 - pair._1 }
  val lineLengths = diffs :+ (snakeLength - diffs.sum)

  // Direction of first line segment is arbitrary; we start with a line going up.
  val rootNode = new Node(Vector(), HashSet(Coordinate(0, 0), Coordinate(0, 1), Coordinate(0, 2)), 2, Coordinate(0, 2), Coordinate(0, 1), new BoundingBox(0, 0, 0, 2), null)

  var currentNode = rootNode
  var lines = lineLengths.tail
  for (turn <- allMoves) {
    currentNode = currentNode.generateChildNode(lines.head, turn.toChar).get
    println(s"${currentNode.moves.size}\t${currentNode.boundingBox}\t${currentNode.boundingBox.ratio(currentNode.length)}")
    lines = lines.tail

    if (currentNode.moves.size == 99) visualize(currentNode, 99)

  }

  def visualize(node: Node, depth: Int): Unit = {
    println(s"Visualizing ${node}")
    PrimeSnakeSolver.main(Array(node.moves.mkString(""), diffs.take(depth).sum.toString))
  }
}
