package snake5000

object Primes extends App {

  def list(max: Int): Seq[Int] = {
    val sieve = new Array[Boolean](max)
    // println(for (i <- 0 until max) yield if (sieve(i)) "." else s"${i}")
    var p = 2
    while (p != -1) {
      for (i <- 2*p until max by p)
        sieve(i) = true
      p = sieve.indexOf(false, p+1)
      // println(for (i <- 0 until max) yield if (sieve(i)) "-" else s"${i}")
    }
    for (i <- 2 until max; if (!sieve(i))) yield i
  }

  println(list(192))
}
