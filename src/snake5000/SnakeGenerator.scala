package snake5000

import java.io.{File, FileWriter}

import primesnakesolver.PrimeSnakeSolver

import scala.collection.immutable.HashSet
import scala.concurrent.ops._
import scala.concurrent.duration.{DurationLong, Duration}

object SnakeGenerator extends App {

  val trace = true
  val traceFile = if (trace) new FileWriter(new File("trace.txt")) else null

  val snakeLength = 5000
  val limitToRatio = 9

  val primes = Primes.list(snakeLength)
  val diffs = (List(0) ++ primes).zip(primes).map { pair => pair._2 - pair._1 }
  val lineLengths = diffs :+ (snakeLength - diffs.sum)
  println(s"Snake with length ${snakeLength} will have ${lineLengths.size} segments with the following sizes: ${lineLengths}")

  val start = System.currentTimeMillis()

  val maxTimeLimit = parseTimeArg(if (args.length > 0) args(0) else "5m")
  val monitor = new Monitor(maxTimeLimit)
  future {
    while (!monitor.done()) {
      print(s"\rBest: ${monitor.best}  Node count: ${monitor.nodeCount} avg: ${monitor.average} n/s ")
      Thread.sleep(500)
    }
  }

  // Direction of first line segment is arbitrary; we start with a line going up.
  val rootNode = new Node(Vector(), HashSet(Coordinate(0, 0), Coordinate(0, 1), Coordinate(0, 2)), 2, Coordinate(0, 2), Coordinate(0, 1), new BoundingBox(0, 0, 0, 2), null)

  // There we go!
  val solutions = solveDepthFirst(rootNode, lineLengths.tail, monitor)

  // Finished, print results.
  val time = System.currentTimeMillis() - start
  println(s"Found ${solutions.size} solutions for snake ${snakeLength} in ${time/1000} seconds; bounding box size is ${solutions(0).boundingBoxSize}.")
  println (s"Node count: ${rootNode.nodeCount()}. Memory usage: ${(Runtime.getRuntime.totalMemory()-Runtime.getRuntime.freeMemory())/(1024*1024)}M")
  visualize(solutions(0))

  def solveDepthFirst(root: Node, lineLengths: List[Int], monitor: Monitor): (List[Node]) = {
    val result = List(root.solve(1, lineLengths, Int.MaxValue, monitor).get)
    monitor.finished()
    result
  }

  def solveBreadthFirst(root: Node, lineLengths: List[Int]): (List[Node]) = {
    var count = 0
    var currentLevel = List(root)
    for (l <- lineLengths) {
      var collectedChilds = List[Node]()
      for (current <- currentLevel) {
        collectedChilds = collectedChilds ++ current.generateChildNodes (l)
      }
      count += collectedChilds.length
      currentLevel = collectedChilds
      print (".")
    }
    println ("")
    println (s"Generated ${count} nodes; ${currentLevel.length} end nodes. Memory usage: ${(Runtime.getRuntime.totalMemory()-Runtime.getRuntime.freeMemory())/(1024*1024)}M")

    val solution = currentLevel.minBy {(node: Node) => node.boundingBoxSize}
    return currentLevel.filter { _.boundingBoxSize == solution.boundingBoxSize }
  }

  def visualize(node: Node): Unit = {
    println(s"Visualizing solution ${node}")
    PrimeSnakeSolver.main(Array(node.moves.mkString(""), snakeLength.toString))
  }

  def visualize(node: Node, depth: Int): Unit = {
    println(s"Visualizing ${node}")
    PrimeSnakeSolver.main(Array(node.moves.mkString(""), diffs.take(depth).sum.toString))
  }

  def parseTimeArg(arg: String): Int = {
    if (arg.endsWith("min")) {
      arg.substring(0, arg.length-3).toInt * 60 * 1000
    }
    else if (arg.endsWith("m")) {
      arg.substring(0, arg.length-1).toInt * 60 * 1000
    }
    else if (arg.endsWith("sec")) {
      arg.substring(0, arg.length-3).toInt * 1000
    }
    else if (arg.endsWith("s")) {
      arg.substring(0, arg.length-1).toInt * 1000
    }
    else if (arg.endsWith("hour")) {
      arg.substring(0, arg.length-4).toInt * 1000
    }
    else if (arg.endsWith("h")) {
      arg.substring(0, arg.length-1).toInt * 60 * 60 * 1000
    }
    else throw new Exception("cannot parse time limit")
  }
}

class Node(val moves: Vector[Char], val snake: HashSet[Coordinate], val length: Int, last: Coordinate, lastDirection: Coordinate, val boundingBox: BoundingBox, parent: Node) {
  var children : List[Node] = List()
  val boundingBoxSize: Int = boundingBox.size
  var computedNodes: Int = -1

  def generateChildNodes(lineLength: Int): List[Node] = {
    children = (for (c <- List(generateChildNode(lineLength, 'L'), generateChildNode(lineLength, 'R')) if (c.isDefined)) yield c).map { _.get }
    children
  }

  def generateChildNode(lineLength: Int, turn: Char): Option[Node] = {
    val newDirection = lastDirection.turn(turn)
    val newCoordinates = generateCoordinates(lineLength, newDirection)
    val inValid = newCoordinates.exists { snake.contains(_) }
    if (! inValid) {
      val newCoordinates = generateCoordinates(lineLength, newDirection)
      Some(new Node(moves :+ turn,
        snake ++ newCoordinates,
        length + newCoordinates.size,
        newCoordinates.last,
        newDirection,
        computeBoundingBox(newCoordinates),
        this))
    }
    else {
      None
    }
  }

  def solve(depth: Int, lineLengths: List[Int], best: Int, progress: Monitor): Option[Node] = {
      generateChildNodes(lineLengths.head)
      if (depth == 1) {
        // At root level, we can limit choices to one, the other is a symmetric mirror
        children = children.tail
      }
      progress.computedNodes(children.size)
      if (lineLengths.size == 1) {
        // Done!
        if (children.nonEmpty) {
          def solution = children.minBy { _.boundingBoxSize }
          trace(s"Found solution ${solution.boundingBoxSize} r=${solution.boundingBox.ratio(length)}")
          Some(solution)
        }
        else
          None
      }
      else {
        var bestSoFar = best
        var solutions = List[Node]()
        for (child <- sort(children)) {
          if (SnakeGenerator.trace) {
            trace(s"${depth}\t${child.boundingBox}")
          }
          if (child.boundingBoxSize < bestSoFar && (SnakeGenerator.limitToRatio <= 0 || child.boundingBox.ratio(length) < SnakeGenerator.limitToRatio)) {
              trace(s"child ratio = ${child.boundingBox.ratio(length)}")
              val solution = if (! progress.outOfTime())
                child.solve(depth + 1, lineLengths.tail, bestSoFar, progress)
              else None
              if (solution.isDefined) {
                solutions = solutions :+ solution.get
                if (solution.get.boundingBoxSize < bestSoFar) {
                  bestSoFar = solution.get.boundingBoxSize
                  progress.registerBestSoFar(bestSoFar)
                  trace(s"${depth} solution ${bestSoFar} + ${solution.get.moves.mkString}")
                }
              }
          }
        }
        computedNodes = 1 + children.map { _.nodeCount() }.sum
        children = null
        if (solutions.nonEmpty)
          Some(solutions.minBy { _.boundingBoxSize })
        else
          None
      }
  }

  def sort(children: List[Node]): List[Node] = {
    if (children.size > 1) {
      if (children(0).boundingBox.area < children(1).boundingBox.area)
        children
      else
        children.reverse
    }
    else
      children
  }

  def generateCoordinates(lineLength: Int, direction: Coordinate): Seq[Coordinate] = {
    (1 to lineLength).map { (l: Int) => last + (direction * l) }
  }

  def computeBoundingBox(newCoordinates: Seq[Coordinate]): BoundingBox = {
    var minx = boundingBox.minx
    var maxx = boundingBox.maxx
    var miny = boundingBox.miny
    var maxy = boundingBox.maxy
    newCoordinates.foreach { c =>
      if (c.x < boundingBox.minx) minx = c.x
      if (c.x > boundingBox.maxx) maxx = c.x
      if (c.y < boundingBox.miny) miny = c.y
      if (c.y > boundingBox.maxy) maxy = c.y
    }
    new BoundingBox(minx, maxx, miny, maxy)
  }

  def nodeCount(): Int = {
    if (computedNodes > -1)
      return computedNodes
    if (children.nonEmpty)
      1 + (children.map { _.nodeCount() }.sum)
    else
      1
  }

  def trace(msg: String) {
    SnakeGenerator.traceFile.write(msg + "\n")
  }

  override def toString = s"${moves.mkString("")} => ${boundingBox}"
}

case class Coordinate(x: Int, y: Int) {

  val xandy = 10000 * x + y

  def +(other: Coordinate) = Coordinate(x + other.x, y + other.y)

  def *(factor: Int) = Coordinate(x * factor, y * factor)

  def turn(turn: Char): Coordinate = {
    turn match {
      case 'L' => {
        this match {
          case Coordinate(1, 0) => Coordinate(0, 1)
          case Coordinate(0, 1) => Coordinate(-1, 0)
          case Coordinate(-1, 0) => Coordinate(0, -1)
          case Coordinate(0, -1) => Coordinate(1, 0)
        }
      }
      case 'R' => {
        this match {
          case Coordinate(1, 0) => Coordinate(0, -1)
          case Coordinate(0, -1) => Coordinate(-1, 0)
          case Coordinate(-1, 0) => Coordinate(0, 1)
          case Coordinate(0, 1) => Coordinate(1, 0)

        }
      }
    }
  }

  override def hashCode() = {
    xandy
  }

  override def toString() = {
    s"(${x},${y})"
  }

  override def equals(obj: scala.Any): Boolean = {
    if (obj.isInstanceOf[Coordinate]) {
      obj.asInstanceOf[Coordinate].xandy == xandy
    }
    else false
  }
}

class BoundingBox(val minx: Int, val maxx: Int, val miny: Int, val maxy: Int) {
  def size = {
    val x = maxx-minx
    val y = maxy-miny
    if (x>y) x else y
  }

  def area = (maxx-minx) * (maxy-miny)

  def ratio(snakeLength: Int): Float = area.asInstanceOf[Float] / snakeLength

  override def toString = {
    s"${maxx-minx}x${maxy-miny} (${minx}:${maxx} ${miny}:${maxy})"
  }
}

class Monitor(val maxTimeLimit: Int = 50 * 1000) {

  var _done = false
  var startTime: Long = System.currentTimeMillis()
  var totalDuration: Duration = null
  var nodeCount: Int = 0
  var best: Int = 0
  var isOutOfTime = false


  def computedNodes(count: Int): Unit = {
    nodeCount += count
  }

  def registerBestSoFar(newBest: Int): Unit = {
    best = newBest
  }

  def average = {
    val duration = (System.currentTimeMillis() - startTime).millis
    (nodeCount / (duration.toMillis / 1000.0)).toInt
  }

  def done() = {
    synchronized {
      _done
    }
  }

  def start() {
    startTime = System.currentTimeMillis()
  }

  def finished() {
    totalDuration = (System.currentTimeMillis() - startTime).millis
    synchronized {
      _done = true
    }
  }
  def duration: Option[Duration] = {
    if (totalDuration != null) Some(totalDuration) else None
  }

  def outOfTime(): Boolean = {
    isOutOfTime || computeOOT()
  }

  def computeOOT(): Boolean = {
    isOutOfTime = System.currentTimeMillis() - startTime > maxTimeLimit
    isOutOfTime
  }
}